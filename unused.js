server.route({
    path: "/convertCURP",
    method: "POST",
    async handler(req, res) {
        const  { curp } = req.payload;
        const browser = await puppeteer.launch({   headless: false});
        let page = await browser.newPage();
        let img = null,
            text = null,
            data = { "requests":[ { "image":{ "content": null }, "features":[ { "type":"TEXT_DETECTION", "maxResults":1}]} ]};

        page.on("response", async (response) => {
            const url = await response.url();
            let buffer = null;
            if(/.*CaptchaServlet.*/.test(url)) {
                buffer = await response.buffer();
                img = buffer.toString("base64");
                data.requests[0].image.content = img;
                const date = new Date().getTime();
                const base64Data = img.replace(/^data:image\/png;base64,/, "");
                await writeFile(date, base64Data);

                const config = {
                    lang: "spa",
                    oem: 1,
                }
                let result = await tesseract.recognize(`./captchas/captcha-${date}.png`, config)
                text = result;
            }
        });

        await Promise.all([
            page.waitForNavigation(),
            page.goto('https://serviciosdigitales.imss.gob.mx/gestionAsegurados-web-externo/asignacionNSS')
        ])

        const input = await page.waitForSelector("#registroCurp");
        const email1 = await page.waitForSelector("#correoInput");
        const email2 = await page.waitForSelector("#correoConfirmacionInput");
        const captchaText = await page.waitForSelector("input[name='captcha']");
        const button = await page.waitForSelector("#continuar")
        
        await input.type(curp);
        await email1.type("sabik.18@gmail.com");
        await email2.type("sabik.18@gmail.com");
        const newText = text.trim().replace(" ", '');
        console.log(`org: ${text}, after: ${newText}`);
        await captchaText.type(newText);
        await Promise.all([
            page.waitForNavigation(),
            button.click(),
        ])
        const url = await page.url();
        if(url == "https://serviciosdigitales.imss.gob.mx/gestionAsegurados-web-externo/asignacionNSS/valida" 
        || url == "https://serviciosdigitales.imss.gob.mx/gestionAsegurados-web-externo/asignacionNSS") {
            await page.close();
            await browser.close();
            return res.response({ message: "error", error: "captcha failed"}, 400);
        }

        // return res.response({ message: "ok", error: "no errors" });
    }
});