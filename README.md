# Timpact PAP
### This is a collection of Node scripts that deal with web scrapping stuff done with Puppeteer as an API built over HapiJS.

* The packages necessary for it to function are on the package JSON
* Optimal NPM and NODE versions
* node ~v11.3.0
* npm ~6.4.1

---

##
### Routes
##
##### Route('/')
@GET
Works as a healtcheck, returns a very simple message in JSON format.

##### Route('/getResults/{id}/{user\_id}/{university\_id}')
@GET
Params: ["id", "user_id", "university\_id"]
The ID is the PAP ID generated by the route /saveResults which is stored on the PG database. The school ID comes from the catalog on the database "university". The user\_id is the id from users which works as a foreign key on the user\_data. The script will go to the PAP main page and retrieve the user information given the id provided and will return either an error or automatically store the information on a mongo database (the production query string is at the start of the index.js file will is also the bootstraper for the HapiJS web app).

##### Route('/saveResults')
@GET
Requires a JSON with the keys userData, additionalData, stepOneData and stepTwoData. The property userData is an object and follows the next schema: 
{ rfc: @user\_rfc, gender: @user\_gender, school\_id: @user\_school\_id }
The additionalData follows the next schema:
{ user\_id: @user\_id, last\_name: @user\_last\_name, name: @user\_name}
The stepOneData and stepTwoData are arrays of the user selections on the PAP step, the ID of the questions and quotes can be found on the Laravel project on the evaluation.js on the resources stuff, which is a Vue instance that makes calls and handles the business logic from the front end.
[{ id: @theIdOfTheQuestion, question: @theQuestion } ... ]

##### Route('/validateAnahuac')
@POST
It requires two params, the users matricula and school password which can be found on the database. Only triggers if the user has the corresponding ID given the school. It only checks that the credentials are valid by  going to the Anahuac webportal and tries to login. If it goes ok, it will return a 200 http status which indicates that the credentials are actually valid.

##### Route('/validateCurp')
@POST
It requires only one param, the CURP to be checked. It launches a headless chrome session and goes to the renapo ws to check for the result, if it works, it stores the data on the database (MongoDB). 

##### Route('/validateEBC')
@POST
It requires two params, the users matricula and school password which can be found on the database. Only triggers if the user has the corresponding ID given the school. It only checks that the credentials are valid by  going to the EBC webportal and tries to login. If it goes ok, it will return a 200 http status which indicates that the credentials are actually valid.


##### Route('/validateUP')
@POST
It requires two params, the users matricula and school password which can be found on the database. Only triggers if the user has the corresponding ID given the school. It only checks that the credentials are valid by  going to the UP webportal and tries to login. If it goes ok, it will return a 200 http status which indicates that the credentials are actually valid.

##### Route('/validateTec')
@POST
It requires two params, the users matricula and school password which can be found on the database. Only triggers if the user has the corresponding ID given the school. It only checks that the credentials are valid by  going to the Tec webportal and tries to login. If it goes ok, it will return a 200 http status which indicates that the credentials are actually valid.

##### Route('/prof/checkCedula')
@POST
It requires two params, the users curp and the users cedula, then it goes to the database (MongoDB) and searches for the cedula profesional given the REAL last name and names stored on the database after the CURP inquiry. Then it saves the result on the database (MongoDB) just if it is successfully found and it matches the cedula provided by the user on the platform.

