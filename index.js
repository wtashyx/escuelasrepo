'use strict';
const PORT = 3002;
const Hapi = require('@hapi/hapi');
const puppeteer = require("puppeteer");
const axios = require("axios");
// const fs = require("fs");
const url = require("url")
// const tesseract = require("node-tesseract-ocr")
const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://timpactUser:KffXniveql8Me2lp@cluster0-hplsd.mongodb.net";



const Sequelize = require('sequelize');
// const db = new Sequelize('timpact', 'timpactAccess', '27JTMavN4RZ4DWG8GJ6kjj7h', {
//     host: '35.192.61.65',
//     dialect: 'postgres',
// });

const db = new Sequelize('timpact', 'postgres', 'password', {
    host: '127.0.0.1',
    dialect: 'postgres',
});

const PAPResult = db.define("pap_result", {
        id: {
            primaryKey: true,
            type: Sequelize.INTEGER,
            autoIncrement: true,
        },
        user_id: Sequelize.INTEGER,
        pap_value: Sequelize.INTEGER,
        pap_term_id: Sequelize.INTEGER,
        created_at: Sequelize.DATE,
        updated_at: Sequelize.DATE,
    }, {
        tableName: "pap_result",
        updatedAt: 'updated_at',
        createdAt: 'created_at',
    });


const UserData = db.define("user_data", {
        id: {
            primaryKey: true,
            type: Sequelize.INTEGER,
            autoIncrement: true,
        },
        is_profesional: Sequelize.BOOLEAN,
        curp: Sequelize.STRING,
        university_id: Sequelize.INTEGER,
        enrollment_id: Sequelize.STRING,
        school_password: Sequelize.STRING,
        dob: Sequelize.DATE,
        gender: Sequelize.INTEGER,
        degree_id: Sequelize.STRING,
        start_year: Sequelize.DATE,
        end_year: Sequelize.DATE,
        user_id: Sequelize.INTEGER,
        pap_token: Sequelize.INTEGER,
    }, {
        tableName: "pap_result",
        updatedAt: 'updated_at',
        createdAt: 'created_at',
    });

const fillUserData = (page, data) => {
    return new Promise(async (res, rej) => {
        const fillData = {
            hasKids: "0",
            hasCouple: "0",
            degree: "8",
            location : "1",
            position: "1",
            seniority: "0",
            state: "0",
            hasDegree: "0",
        }
        const user_id = data.user_id;

        const lastNameFirstInput = await page.waitForSelector("input[name='Paterno']", { timeout: 0});
        const lastNameSecondInput = await page.waitForSelector("input[name='Materno']");
        const fullNameInput = await page.waitForSelector("input[name='NombreE']");
        const hasKidsSelect = await page.waitForSelector("select[name='HijosSiNo']");
        const hasCoupleSelect = await page.waitForSelector("select[name='Pareja']");
        const scholarShipSelect = await page.waitForSelector("select[name='Escolar']");
        const locationSelector = await page.waitForSelector("select[name='TiendaRegion']");
        const positionSelector = await page.waitForSelector("select[name='EmpPosition']");
        const senioritySelector = await page.waitForSelector("select[name='Antiguo']");
        const stateSelector = await page.waitForSelector("select[name='Estado']")
        const hasDegreeSelector = await page.waitForSelector("select[name='Licenciatura']")

        await lastNameFirstInput.type(data.lastName1);
        await lastNameSecondInput.type(data.lastName2);
        await fullNameInput.type(data.name);

        await hasKidsSelect.select(fillData.hasKids);
        await hasCoupleSelect.select(fillData.hasCouple);
        await scholarShipSelect.select(fillData.degree);
        await locationSelector.select(fillData.location);
        await positionSelector.select(fillData.position);
        await senioritySelector.select(fillData.seniority);
        await stateSelector.select(fillData.state);
        await hasDegreeSelector.select(fillData.hasDegree);

        const buttonSubmit = await page.waitForSelector("input[type='submit']")

        await Promise.all([
            page.waitForNavigation(),
            buttonSubmit.click(),
        ]);

        const url = await page.url();
        // console.log(url)
        const getFicha = url.match(/^.*EMP_FICHA=(\d+).*$/);
        // console.log(getFicha)
        const ficha = parseInt(getFicha[1]);
	console.log('finished registering user additional data');

        await db.query(`UPDATE user_data SET pap_token = ${ficha} WHERE user_id = ${user_id}`)
        
        res(page);
    });
}

const fillPhrases = (page, phraseData) => {
    return new Promise(async (res, rej) => {
        const t1Input = await page.waitForSelector("input[name='T1'");
        const t2Input = await page.waitForSelector("input[name='T2'");
        const t3Input = await page.waitForSelector("input[name='T3'");
        const t4Input = await page.waitForSelector("input[name='T4'");
        const t5Input = await page.waitForSelector("input[name='T5'");
        const t6Input = await page.waitForSelector("input[name='T6'");
        const t7Input = await page.waitForSelector("input[name='T7'");
        const t8Input = await page.waitForSelector("input[name='T8'");
        const t9Input = await page.waitForSelector("input[name='T9'");
        const t10Input = await page.waitForSelector("input[name='T10'");
        const t11Input = await page.waitForSelector("input[name='T11'");
        const t12Input = await page.waitForSelector("input[name='T12'");
        const t13Input = await page.waitForSelector("input[name='T13'");
        const t14Input = await page.waitForSelector("input[name='T14'");
        const t15Input = await page.waitForSelector("input[name='T15'");
        const t16Input = await page.waitForSelector("input[name='T16'");
        const t17Input = await page.waitForSelector("input[name='T17'");
        const t18Input = await page.waitForSelector("input[name='T18'");

        console.log("hereeee")
        console.log(phraseData)
        console.log("hereeee")

        await t1Input.type(phraseData["T1"].toString());
        await t2Input.type(phraseData["T2"].toString());
        await t3Input.type(phraseData["T3"].toString());
        await t4Input.type(phraseData["T4"].toString());
        await t5Input.type(phraseData["T5"].toString());
        await t6Input.type(phraseData["T6"].toString());
        await t7Input.type(phraseData["T7"].toString());
        await t8Input.type(phraseData["T8"].toString());
        await t9Input.type(phraseData["T9"].toString());
        await t10Input.type(phraseData["T10"].toString());
        await t11Input.type(phraseData["T11"].toString());
        await t12Input.type(phraseData["T12"].toString());
        await t13Input.type(phraseData["T13"].toString());
        await t14Input.type(phraseData["T14"].toString());
        await t15Input.type(phraseData["T15"].toString());
        await t16Input.type(phraseData["T16"].toString());
        await t17Input.type(phraseData["T17"].toString());
        await t18Input.type(phraseData["T18"].toString());

        const submitButton = await page.waitForSelector("input[type='submit']")
        await Promise.all([
            page.waitForNavigation(),
            submitButton.click(),
        ]);
	console.log('finished registering page pap 1');

        res(page);
    });
}

const fillQuotes = (page, phraseData) => {
    return new Promise(async (res, rej) => {
        const t1Input = await page.waitForSelector("input[name='T1'");
        const t2Input = await page.waitForSelector("input[name='T2'");
        const t3Input = await page.waitForSelector("input[name='T3'");
        const t4Input = await page.waitForSelector("input[name='T4'");
        const t5Input = await page.waitForSelector("input[name='T5'");
        const t6Input = await page.waitForSelector("input[name='T6'");
        const t7Input = await page.waitForSelector("input[name='T7'");
        const t8Input = await page.waitForSelector("input[name='T8'");
        const t9Input = await page.waitForSelector("input[name='T9'");
        const t10Input = await page.waitForSelector("input[name='T10'");
        const t11Input = await page.waitForSelector("input[name='T11'");
        const t12Input = await page.waitForSelector("input[name='T12'");
        const t13Input = await page.waitForSelector("input[name='T13'");
        const t14Input = await page.waitForSelector("input[name='T14'");
        const t15Input = await page.waitForSelector("input[name='T15'");
        const t16Input = await page.waitForSelector("input[name='T16'");
        const t17Input = await page.waitForSelector("input[name='T17'");
        const t18Input = await page.waitForSelector("input[name='T18'");

        await t1Input.type(phraseData["T1"].toString());
        await t2Input.type(phraseData["T2"].toString());
        await t3Input.type(phraseData["T3"].toString());
        await t4Input.type(phraseData["T4"].toString());
        await t5Input.type(phraseData["T5"].toString());
        await t6Input.type(phraseData["T6"].toString());
        await t7Input.type(phraseData["T7"].toString());
        await t8Input.type(phraseData["T8"].toString());
        await t9Input.type(phraseData["T9"].toString());
        await t10Input.type(phraseData["T10"].toString());
        await t11Input.type(phraseData["T11"].toString());
        await t12Input.type(phraseData["T12"].toString());
        await t13Input.type(phraseData["T13"].toString());
        await t14Input.type(phraseData["T14"].toString());
        await t15Input.type(phraseData["T15"].toString());
        await t16Input.type(phraseData["T16"].toString());
        await t17Input.type(phraseData["T17"].toString());
        await t18Input.type(phraseData["T18"].toString());

        const submitButton = await page.waitForSelector("input[type='submit']")
        await Promise.all([
            page.waitForNavigation(),
            submitButton.click(),
        ]);
	console.log('finished registering page pap 2');

        res(page);
    });
}

const login = (page) => {
    return new Promise(async (res, rej) => {        
        const user = "Christop";
        const pass = "CH_uni1";

        const loginInput = await page.waitForSelector("input[name='login']");
        const passInput = await page.waitForSelector("input[name='password']");
        const submit = await page.waitForSelector("input[type='submit']");

        await loginInput.type(user)
        await passInput.type(pass)

        await Promise.all([
            page.waitForNavigation(),
            submit.click(),
        ]);

        res(page);
    });
}

const gotoResults = (page, id, univid) => {
    return new Promise(async (res, rej) => {
        const btn = await page.waitForSelector("[href='ResultadosConf/SelAreas.asp']");
        await Promise.all([
            page.waitForNavigation(),
            btn.click(),
        ]);

        const universities = {
            1: "U. ANAHUAC",
            2: "U. ANAHUAC",
            // 2: "U. IBERO",
            // 3: "U. LA SALLE",
            // 5: "U. VALLE DE MEXICO",
        };
        const university = universities[univid];

        const btn2 = await page.waitForSelector(`[href='SelecionTienda.asp?IdArea=1&NombreArea=${university}']`);
        await Promise.all([
            page.waitForNavigation(),
            btn2.click(),
        ]);
        
        const btn3 = await page.waitForSelector("[href='ver_empleados_con_pap_x_TyP.asp?Id_Tienda=1&IdArea=1']");
        await Promise.all([
            page.waitForNavigation(),
            btn3.click(),
        ]);

        const btn4 = await page.waitForSelector(`[href='Demo1Empl_nvo.asp?Ficha=${id}']`);
        await Promise.all([
            page.waitForNavigation(),
            btn4.click(),
        ]);

        const btn5 = await page.waitForSelector(`[href='AxioPerfBase_Wal_ESP.asp?Ficha=${id}']`);
        await Promise.all([
            page.waitForNavigation(),
            btn5.click(),
        ]);

        res(page);

    });
}

const insertOne = (el, userId) => {
    return new Promise((res, rej) => {
        let result;
        try {
            result = PAPResult.create({
                user_id: userId,
                pap_term_id: el.id,
                pap_value: el.value,
            });
            res(result);
        } catch(e) {
            console.log(e);
        }
    });
}

const registerUser = (page, data) =>  {
    return new Promise(async (res, rej) => {
        const employeesButton = await page.waitForSelector("a[href='CapEmpl1.asp']");
        await Promise.all([
            page.waitForNavigation(),
            employeesButton.click(),
        ]);

        const rfcInput = await page.waitForSelector("input[name='RFC']");
        const genderSelect = await page.waitForSelector("select[name='Sexo']");
        const employeeSelect = await page.waitForSelector("select[name='TipoEmpleado']");
        const schoolSelect = await page.waitForSelector("select[name='Area']");
        const submitButton = await page.waitForSelector("input[type='submit']");

        // 1 anahuac , 4  iBero , 3 la salle, 2 uvm
        const universities = { 1 : 1, 2 : 4, 3 : 3, 5 : 2, };
        const university = 1; // universities[data.schoolId] || -1;
        // genders 1 fem, 2 masc
        const genders = { 2: 1, 1: 2};
        const gender = genders[data.gender];
        // types:  -1 candidate, 0 active
        const type = -1;
        const rfc = data.rfc;

        await rfcInput.type(rfc);
        await genderSelect.select(gender.toString());
        await employeeSelect.select(type.toString());
        await schoolSelect.select(university.toString());
        await page.waitFor(100000)

        await Promise.all([
            page.waitForNavigation(),
            submitButton.click(),
        ])
	console.log('finished register user')
        res(page)
    });
}

const init = async () => {
    const server = Hapi.server({
        port: PORT,
        host: "127.0.0.1",
    });

    /*
    @Params null
    Healthcheck
    */
    server.route({
       method: "GET",
       path:"/",
       async handler(req, res){
           return res.response({msg: "hello"})
       }
    });


    /*
    @Params
    Retrieve the user's result given the user_id
    id              @Integer
    user_id         @Integer
    @university_id  @Integer
    */
    server.route({
        method: "GET",
        path: '/getResults/{id}/{user_id}/{university_id}',
        async handler(req, res) {
            const browser = await puppeteer.launch({ headless: true, dumpio: true,  args:['--no-sandbox', '--disable-setuid-sandbox']});
            let page = await browser.newPage();
            await page.goto('http://qualinort.net/login_Acc.asp');
            page = await login(page);
            page = await gotoResults(page, req.params.id, req.params.university_id);

            const values = await page.evaluate(() => {
                var data = [];
                var inddex = 0;
                var x = document.querySelectorAll("table table tbody tr")
                Array.from(x).forEach((el, index) => {
                    if(index === 0 || index === 1) return;
                    if(el.length === 0) return;
                    Array.from(el.children).forEach((ch, ind) => {
                        if(ind != 3) return;
                        if(ch.children.length == 0) return;
                        let numb = ch.firstChild.innerText.replace("%", '');
                        data.push({ value: parseFloat(numb), id: ++inddex });
                    });
                });
                return data;
            });
            
            for(let value of values){
                await insertOne(value, req.params.user_id);
            }

            await browser.close();
            return res.response({ success: true, message: "saved" });
        }
    });

    /*
    @Params
    Save the user's result given the user_id
    userData            @Object
    additionalData      @Object
    stepOneData         @Object
    stepTwoData         @Object
    */
    server.route({
        method: "POST",
        path: "/saveResults",
        async handler(req, res) {
            console.log(JSON.stringify(req.payload))
            const { userData, additionalData, stepOneData, stepTwoData } = req.payload;
	    console.log('here, failed something')
            // const browser = await puppeteer.launch({ headless: true, dumpio: true,  args:['--no-sandbox', '--disable-setuid-sandbox']});
            const browser = await puppeteer.launch({ headless: false, defaultViewport: { width: 1920, height: 1080}});
	    console.log('here, launched')
            let page = await browser.newPage();
	    console.log('here, starting')
            await page.goto('http://qualinort.net/login_Acc.asp');
	    console.log('here, visited')
            page = await login(page);
	    console.log('here, logged')
            page = await registerUser(page, userData);
            console.log('here, registered user')
            page = await fillUserData(page, additionalData);
	    console.log('here, registered additional')
            page = await fillPhrases(page, stepOneData);
   	    console.log('here, registered pap 1')
            page = await fillQuotes(page, stepTwoData);
  	    console.log('here, registered pap 2')
            await browser.close();
            return res.response({ success: true, message: "saved"});
        }
    });

    /*
    @Params
    Validate the users login on the Anahuac portal
    matricula     @String
    password      @String
    */
    server.route({
        path: "/validateAnahuac",
        method: "POST",
        async handler(req, res) {
            const { matricula, password } = req.payload;
            const browser = await puppeteer.launch({ headless: true, dumpio: true,  args:['--no-sandbox', '--disable-setuid-sandbox']});
            let page = await browser.newPage();
            console.log(` matricula : ${ matricula }, password: ${password}`)
            await page.goto('https://rua-ssb-prod.ec.lcred.net/pls/UAN/twbkwbis.P_WWWLogin');
            const userNameInput = await page.waitForSelector("#UserID");
            const passwordInput = await page.waitForSelector("input[name='PIN']");
            const button = await page.waitForSelector("input[type='submit']");

            await userNameInput.type(matricula);
            await passwordInput.type(password);

            await Promise.all([
                page.waitForNavigation(),
                button.click(),
            ]);

            try {
                const alumnosHREF = await page.waitForSelector("a[href='/pls/UAN/twbkwbis.P_GenMenu?name=bmenu.P_StuMainMnu']", { timeout: 2000 });
                await Promise.all([
                    page.waitForNavigation(),
                    alumnosHREF.click(),
                ]);
                await page.close();
                await browser.close();
                return res.response({ message: "success", errors: null, });
            } catch(e) {
                await page.close();
                await browser.close();
                return res.response({message: "error", errors: "invalid_credentials" }, 400);
            }

            // try {
            //     const information = await page.waitForSelector("a[href='/pls/UAN/twbkwbis.P_GenMenu?name=bmenu.P_AdminMnu']", { timeout: 2000});
            //     await Promise.all([
            //         page.waitFornNavigation(),
            //         information.click(),
            //     ]);
            // } catch(e) {
            //     return res.response({ message: "error", errors: "cant_access"  }, 400);
            // }


            // try {
            //     const academic = await page.waitForSelector("a[href='/pls/UAN/bwskotrn.P_ViewTermTran']", { timeout: 2000 });
            //     await Promise.all([
            //         page.waitForNavigation(),
            //         academic.click(),
            //     ]);
            // } catch(e){
            //     return res.response({ message: "error", errors: "cant_access"}, 500);
            // }

            // try {
            //     const selector  = await page.waitForSelector("#levl_id", { timeout: 2000});
            //     await Promise.all([
            //         page.waitForNavigation(),
            //         selector.click(),
            //     ]);
            // } catch(e) {
            //     return res.response({message: "error", errors: "cant_access" }, 2000);
            // }

            return res.response({ message: "success", data: "valid credentials"});

        }
    });

    /*
    @Params
    Validate the users login on the Anahuac portal
    curp     @String
    */
    server.route({
        method: "POST",
        path: "/validateCurp",
        async handler(req, res) {
            const {curp} = req.payload;
            // const browser = await puppeteer.launch({   headless: true});
            const browser = await puppeteer.launch({ headless: true, dumpio: true,  args:['--no-sandbox', '--disable-setuid-sandbox']});
            let page = await browser.newPage();
            
            try {
                await page.goto("http://www.renapo.sep.gob.mx/wsrenapo/");
            } catch(e) {
                await page.close();
                await browser.close();
                return res.response({ message: "error", errors: "could not connect to renapo"}, 500)
            }
            const input = await page.waitForSelector("input[name='curp']");
            const button = await page.waitForSelector("input[type='submit']");
            await input.type(curp);
            console.log('1')
            await Promise.all([
                page.waitForNavigation(),
                button.click()
            ]);

            let result = await page.evaluate(() => {
                let elements = document.getElementsByTagName("tr");
                if(elements.length < 5)
                    return false;

                let results = {};
                Array.from(elements).forEach((el, index) => {
                    if(index === 0) return;
                    results[el.children[0].innerText.replace(":", "")] = el.children[1].innerText;
                });

                return results;
            });
            console.log('2')

            const client = new MongoClient(uri);

            try {
                await client.connect();
                console.log("Connected correctly to server");
                const db = client.db("timpact");
                await db.collection('curp').insertOne(result);

            } catch (err) {
                console.log(err.stack);
            }
            
            console.log(result)

            if(result === false){
                await page.close();
                await browser.close();
                return res.response({ "message": "error", "data": "invalid curp"}, 400);
            }

            await page.close();
            await browser.close();
            return res.response({ message: "success", data: "ok" });
            
        }
    });


    /*
    @Params
    Validate the users login on the EBC portal
    matricula     @String
    password      @String
    */
    server.route({
        path: "/validateEBC",
        method: "POST",
        async handler(req, res) {
            const {matricula, password} = req.payload;
            const browser = await puppeteer.launch({ headless: true, dumpio: true,  args:['--no-sandbox', '--disable-setuid-sandbox']});
            let page = await browser.newPage();
            await page.goto("https://apr.ebc.edu.mx/casad/login?service=https%3A%2F%2Fintranet.ebc.edu.mx%2Fc%2Fportal%2Flogin");
            
            const user = await page.waitForSelector("input#username");
            const pass = await page.waitForSelector("input#password");
            const button = await page.waitForSelector("#botonSumitLogin");

            await user.type(matricula);
            await pass.type(password);

            await Promise.all([
                page.waitForNavigation(),
                button.click(),
            ]);

            try {
                const inside = await page.waitForSelector("#column-1", { timeout: 0 });
                await page.close();
                await browser.close();
                return res.response({ message: "success", errors: null, });

            } catch(e) {
                await page.close();
                await browser.close();
                return res.response({message: "error", errors: "invalid_credentials" }, 400);
            }
            
            return res.response({ message: "error", error: "unknown failure"}, 500);

        }
    });


    /*
    @Params
    Validate the users login on the UP portal
    matricula     @String
    password      @String
    */
    server.route({
        method: "POST",
        path: "/validateUP",
        async handler(req, res) {
            const { matricula, password } = req.payload;
            const browser = await puppeteer.launch({ headless: true, dumpio: true,  args:['--no-sandbox', '--disable-setuid-sandbox']});
            let page = await browser.newPage();
            await page.goto("https://www.upsite.upmx.mx/psp/Campus/?cmd=login&languageCd=ESP");

            const user = await page.waitForSelector("#userid");
            const passwrdInput = await page.waitForSelector("#pwd");
            const button = await page.waitForSelector("input[type='submit']");

            await user.type(matricula);
            await passwrdInput.type(password);

            await Promise.all([
                page.waitForNavigation(),
                button.click(),
            ]);

            try {
                const inside = await page.waitForSelector("#mi-evaluacion");
                await page.close();
                await browser.close();
                return res.response({ message: "success", errors: null, });

            } catch(e) {
                await page.close();
                await browser.close();
                return res.response({message: "error", errors: "invalid_credentials" }, 400);
            }

            return res.response({ message: "error", error: "unknown failure"}, 500);
        }
    });


    /*
    @Params
    Validate the users login on the Anahuac portal
    curp        @String
    cedula      @String
    */
    server.route({
        path: "/prof/checkCedula",
        method: "POST",
        async handler(req, res) {
            const { curp, cedula } = req.payload;
            if(!curp || !cedula)
                return res.response({ result: "error", message: "missing params" }, 400);
            
            try {
                let client = new MongoClient(uri, { useUnifiedTopology: true }),
                    cedulas = null,
                    items = [],
                    callResult = null,
                    params = null,
                    notCalled = false,
                    index = 0,
                    found = null;

                await client.connect();
                const db = client.db("timpact");

                found = await db.collection('curp').findOne({ CURP: curp });
                if(!found)
                    return res.response({  result: "error", message: "curp not found"}, 404);

                const { Apellido1, Apellido2, Nombres } = found;
                do {
                    try {
                        console.log("test ",index)
                        params = new url.URLSearchParams();
                        params.append("json", `{ maxResult: "1000", nombre :"${Nombres}", paterno: "${Apellido1}", materno: "${Apellido2}", idCedula: "${cedula}"}`);
                        callResult = await axios.post("https://www.cedulaprofesional.sep.gob.mx/cedula/buscaCedulaJson.action", params, { timeout: 4000 });
                        items = callResult.data.items;
                        notCalled = true;
                    } catch(e) {
                        index++;
                    }
                } while(notCalled === false);

                cedulas = Array.from(items).filter((el) => {
                    return el.idCedula == cedula;
                });

                if(cedulas && cedulas.length > 0){
                    await db.collection("cedulas").insertOne(cedulas[0]);
                    client.close();
                    return res.response({ message: "success", message: "cedula inserted"}, 200);
                } else {
                    client.close();
                    return res.response({ message: "error", message: "cedula not found"}, 404)
                }
            } catch(e) {
                console.log(e)
                return res.response({ message: "success", message: "could not connect to db", stack: e.stack });
            }
        }
    })


    /*
    @Params
    Validate the users login on the Tec portal
    matricula     @String
    password      @String
    */
    server.route({
        path: "/validateTec",
        method: "POST",
        async handler(req, res){
            const { matricula, password } = req.payload;
            const browser = await puppeteer.launch({ headless: true, dumpio: true,  args:['--no-sandbox', '--disable-setuid-sandbox']});
            let page = await browser.newPage();
            await page.goto("https://fs.itesm.mx/adfs/ls/?wa=wsignin1.0&wtrealm=urn%3asharepoint%3amitec&wctx=https%3a%2f%2fmitec.itesm.mx%2f_layouts%2f15%2fAuthenticate.aspx%3fSource%3d%252F");

            const user = await page.waitForSelector("#userNameInput");
            const passwrdInput = await page.waitForSelector("#passwordInput");
            const button = await page.waitForSelector("#submitButton");

            await user.type(matricula);
            await passwrdInput.type(password);

            await Promise.all([
                page.waitForNavigation(),
                button.click(),
            ]);

            try {
                const inside = await page.waitForSelector("#mi-evaluacion");
                await page.close();
                await browser.close();
                return res.response({ message: "success", errors: null, });

            } catch(e) {
                await page.close();
                await browser.close();
                return res.response({message: "error", errors: "invalid_credentials" }, 400);
            }

            return res.response({ message: "error", error: "unknown failure"}, 500);
        }
    });

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

init();
